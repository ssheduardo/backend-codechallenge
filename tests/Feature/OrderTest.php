<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Tests\TestCase;
use App\Entities\Order;

use App\Entities\Driver;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class OrderTest extends TestCase
{
    /*  public function setUp(): void
    {
        parent::setUp();
        //Refresh database
        $this->artisan('migrate:refresh');

    } */

    /**
     * Create an order.
     * @test
     */
    public function it_create_order()
    {
        factory(Driver::class, 10)->create();
        $order = factory(Order::class)->make();

        $response = $this->json('POST', route('api.v1.order.store'), $order->toArray());

        $response
            ->assertJson([
                'data' => [
                    'created' => true,
                ]
            ])
            ->assertStatus(201);
    }

    /**
     * Test fail
     * @test
    */

    public function it_test_fail_when_send_all_required_data_empty()
    {
        $order = [];
        $response = $this->json('POST', route('api.v1.order.store'), $order);
        $response
            ->assertJson(
                [
                    'message' => 'The given data was invalid.',
                    'errors' => [
                        'driver_id' => [
                            0 => 'The driver id field is required.',
                        ],
                        'name' => [
                            0 => 'The name field is required.',
                        ],
                        'lastname' => [
                            0 => 'The lastname field is required.',
                        ],
                        'email' => [
                            0 => 'The email field is required.',
                        ],
                        'phone' => [
                            0 => 'The phone field is required.',
                        ],
                        'date_delivery' => [
                            0 => 'The date delivery field is required.',
                        ],
                        'hour' => [
                            0 => 'The hour field is required.',
                        ],

                    ],
                ]
            )
            ->assertStatus(422);
            //->decodeResponseJson();
    }


    /**
     * Get all orders, set driver id and date delivery
     * @test
     */
    public function get_orders_through_driver_and_date_delivery()
    {
        factory(Driver::class, 5)->create();
        factory(Order::class, 50)->create();
        $driver_id = 1;
        $date = Carbon::now()->toDateString();

        $response = $this->json( 'GET', route('api.v1.order.orders', ['driver_id' => $driver_id, 'date' => $date]) );

        $response
            ->assertJson([
                'data' => [
                    0 => [
                        'driver_id' => $driver_id,
                        'date_delivery' => $date
                    ]
                ]
            ])
            ->assertStatus(200);

    }

    /**
     * Test return empty orders
     * @test
     */
    public function it_test_return_empty_orders()
    {
        $driver_id = 2;
        $date = Carbon::now()->toDateString();

        $response = $this->json( 'GET', route('api.v1.order.orders', ['driver_id' => $driver_id, 'date' => $date]) );

        $response
            ->assertJson([
                'data' => [],
            ])
            ->assertStatus(200)->decodeResponseJson();
    }
}
