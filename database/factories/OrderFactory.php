<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Carbon\Carbon;
use App\Entities\Order;
use App\Entities\Driver;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'uuid'          => $faker->uuid,
        'driver_id'     => Driver::all()->random()->id,
        'name'          => $faker->firstName,
        'lastname'      => $faker->lastname,
        'email'         => $faker->unique()->safeEmail,
        'phone'         => $faker->e164PhoneNumber,
        'address'       => $faker->streetAddress,
        'date_delivery' => Carbon::now()->toDateString(),
        'hour'          => interval_delivery(),
    ];
});
