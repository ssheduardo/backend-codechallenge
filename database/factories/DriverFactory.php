<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use App\Entities\Driver;
use Faker\Generator as Faker;

$factory->define(Driver::class, function (Faker $faker) {
    return [
        'uuid'          => $faker->uuid,
        'name'          => $faker->firstName,
    ];
});
