<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('v1')->namespace('Api')->group(function () {
    Route::post('order','OrderController@store')->name('api.v1.order.store');
    Route::get('order/{driver_id}/{date}', 'OrderController@orders')->name('api.v1.order.orders');
});


/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/
