<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'driver_id'     => 'required',
            'name'          => 'required',
            'lastname'      => 'required',
            'email'         => 'required|email',
            'phone'         => 'required',
            'date_delivery' => 'required',
            'hour'          => 'required',
        ];
    }
}
