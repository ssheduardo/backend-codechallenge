<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrdersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'uuid'                  => $this->uuid,
            'driver_id'              => $this->driver_id,
            'name'                  => $this->name,
            'lastname'              => $this->lastname,
            'email'                 => $this->email,
            'phone'                 => $this->phone,
            'address'               => $this->address,
            'date_delivery'         => $this->date_delivery,
            'hour'                  => $this->hour,
        ];
    }
}
