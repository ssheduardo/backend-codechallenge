<?php

namespace App\Http\Controllers\Api;

use App\Entities\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Http\Resources\OrdersResource;
use App\Http\Requests\Api\OrderRequest;

class OrderController extends Controller
{
    /**
     * @var Order
     */
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function store(OrderRequest $request)
    {
        Log::info($request->all());
        $order = $this->order->create($request->all());
        return new OrderResource($order);
    }

    public function orders($driver_id='', $date='')
    {
        Log::info( 'Driver: ' . $driver_id . ' -> ' . $date );
        $orders = $this->order
            ->where('driver_id',$driver_id)
            ->where('date_delivery', $date)
            ->get();
        Log::info( $orders->toArray() );
        return  OrdersResource::collection($orders);

    }
}
