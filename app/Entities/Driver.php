<?php

namespace App\Entities;

use App\Entities\Order;
use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $fillable = ['uuid','name'];


    public function orders()
    {

        return $this->belongsTo(Order::class);

    }
}
