<?php

namespace App\Entities;

use App\Entities\Driver;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'driver_id',
        'uuid',
        'name',
        'lastname',
        'email',
        'phone',
        'address',
        'date_delivery',
        'hour'
    ];


    public function driver()
    {
        return $this->hasMany(Driver::class);
    }
}
