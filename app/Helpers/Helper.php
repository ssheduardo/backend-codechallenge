<?php

if (!function_exists('interval_delivery')) {
    function interval_delivery()
    {
        $interval = [
            '09:00 - 11:00',
            '11:00 - 14:00',
            '14:00 - 17:00',
            '17:00 - 20:00',
        ];
        return $interval[ rand(0, count($interval) -1 ) ];
    }
}
